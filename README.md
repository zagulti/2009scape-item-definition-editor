**The 2009scape Item Definition Editor : For your cache editing needs**

This tool is capable of modifying every item definition inside of the 2009scape cache.
This tool is intended only for developers wanting a custom 2009scape experience.
If you are running your own fork of the codebase please do not report your bugs to me.

This repository is open for merge requests (Looking mostly for UI work).

<br>

**Information:**

- Do **NOT** make a merge request to the main 2009scape repository after using this tool. If cache files are changed, your merge request will be instantly denied.
- Do **NOT** request more cache editing tools.
- This tool will **ONLY** work on cache revision 530, **OSRS** and **ANY OTHER CACHE REVISIONS ARE NOT SUPPORTED**.
- If you are serious about learning the inner workings of the cache and data manipulation contact woahscam#0001 on Discord.
